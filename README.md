# POM 2022 2023

# Auteurs

ATBIR Hind, BESSY Axel 

**Encadrant** : Remy Cazabet

# Objectif

Le but de ce projet est d'utiliser des méthodes de meta-learning pour faire de la prédiction de liens sur
les graphes en se basant sur des algorithmes de détection de communautés (qui détectent les parties plus denses d'un graphe).

Le meta-learning permet de combiner les prédictions de plusieurs algorithmes pour obtenir de meilleures prédictions.
Il y a trois grandes méthodes de meta-learning :

- *Bagging* : combine les résultats d'algorithmes appliqués à différentes sous-parties du graphe.
- *Boosting* : combine les résultats d'un algorithme de manière itérative.
- *Stacking* : combine les résultats d'algorithmes appliqués sur tout le graphe.

Nous nous sommes principalement focalisé·es sur le stacking durant ce projet.

# Travail Effectué 

## Préparation des données

Nous testons nos résultats sur trois graphes de différentes tailles.
- *Karate_club* : 34 nœuds - 78 liens
- *Dolphins* : 62 nœuds - 154 liens
- *Hamsterster* : 2426 nœuds - 16629 liens

Pour tester nos algorithmes de prédiction de liens, un certain pourcentage des liens est retiré du graphe
(25%). Ces liens constitueront l'ensemble des liens manquants (qui devrait être prédits "positivement" par l'algorithme).
Ensuite, un certain nombre de non-liens dans le graphe est tiré (Suffisamment, pour qu'il y ait environ 400 liens sur lesquels
lancer nos algorithmes pour *karate_club*, 1000 pour *dolphins* et 7000 pour *hamstertser*). Ces liens devraient être
prédits "négativement" par les algorithmes.

## Prédiction de lien 

Nous avons implémenté quatre fonctions de prédiction de liens qui se basent sur des algorithmes de
détection de communautés. Chacune calcule un score sur un ensemble de liens donnés. Plus ce score est haut, plus 
il y a de chances que le lien existe. Chaque fonction commence par effectuer une détection de communauté avec l'algorithme
de louvain ou l'algorithme infomap.

- ``lp_louvain_diff`` : la fonction de score est la différence de modularité de louvain avant et
après l'ajout d'un lien.
- ``lp_infomap_diff`` : la fonction de score est la différence de résultat de la map Equation utilisée dans l'algorithme
infomap après et avant l'ajout d'un lien.
- ``lp_louvain_density`` : si le lien se trouve à l'interieur d'une communauté alors la fonction de score sera
la densité au sein de cette communauté ($nbLiens \over nbLiensPossibles$) sinon, ce sera la densité 
entre ces deux communautés. Le calcul de communautés se fait avec l'algorithme de louvain.
- ``lp_infomap_density`` : si le lien se trouve à l'intérieur d'une communauté alors la fonction de score sera
la densité au sein de cette communauté ($nbLiens \over nombre de liensPossibles$) sinon, ce sera la densité 
entre ces deux communautés. Le calcul de communautés se fait avec l'algorithme infomap.

## Stacking

Nous utilisons du stacking pour combiner les prédictions données par nos algorithmes de prédiction de liens.
Nous avons implémenté trois fonctions de stacking qui combinent les prédictions de manière différente :
- ``StackingMean`` : fait une simple moyenne sur les prédictions standardisées.
- ``StackingWeightedMean`` : fait une moyenne pondérée sur les prédictions standardisées. On teste au préalable
chaque algorithme sur la partie connue du graphe, les poids sont calculés en fonction de l'AUC de chacun des
algorithmes.
- ``StackingDT`` : utilise un meta-model (arbre de décision) pour combiner les prédictions. L'arbre de décision est
entrainé sur les résultats de nos algorithmes sur tout le reste du graphe (la partie connue) pour ensuite prédire la 
probabilité qu'un lien existe.

Malheureusement, les résultats que nous obtenons avec la fonction ``StackingDT`` ne sont pas à la hauteur de ce
que nous espérions. Nous comptons continuer à travailler sur ce projet pour trouver d'où vient le problème en
dehors du cadre de cette UE.


## Boosting 

Nous avons tenté de faire du boosting adaptatif en s'inspirant de l'algorithme Adaboost. Nous ne sommes pas
parvenu·es à trouver une solution intéressante dans le cadre de cette UE, mais nous continuerons à travailler sur
cet aspect.

## Evaluation

Pour tester nos algorithmes, nous utilisons une méthode de validation croisée. Les algorithmes sont exécutés
un certain nombre de fois (20 sur les petits graphes, 8 sur les plus gros). À chaque exécution, 25% du graphe 
est retiré aléatoirement et les non-liens à prédire sont aussi tirés aléatoirement. 

Le score final de l'algorithme est la moyenne des AUC obtenues à chaque exécution.

# Résultats

Les résultats peuvent être trouvés dans le dossier ``results`` sous forme d'un tableau en pdf.
Une version latex peut être générée en exécutant le code du notebook.

# Bibliographie

<div class="csl-entry">Fortunato, S. (2010). Community detection in graphs. <i>Physics Reports</i>, <i>486</i>(3–5), 75–174. https://doi.org/10.1016/J.PHYSREP.2009.11.002</div>  
&nbsp;
<div class="csl-entry">Rosvall, M., &#38; Bergstrom, C. T. (2008). <i>Maps of random walks on complex networks reveal community structure</i>. www.pnas.org/cgi/content/full/</div>
&nbsp;
<div class="csl-entry">Ghasemian, A., Hosseinmardi, H., &#38; Clauset, A. (n.d.). <i>Evaluating Overfit and Underfit in Models of Network Community Structure</i>.</div>
&nbsp;
<div class="csl-entry">Yang, Y., Lichtenwalter, R. N., &#38; Chawla, N. v. (2015). Evaluating link prediction methods. <i>Knowledge and Information Systems</i>, <i>45</i>(3), 751–782. https://doi.org/10.1007/s10115-014-0789-0</div>
&nbsp;
<div class="csl-entry">Ghasemian, A., Hosseinmardi, H., Galstyan, A., Airoldi, E. M., &#38; Clauset, A. (2020). Stacking models for nearly optimal link prediction in complex networks. <i>Proceedings of the National Academy of Sciences of the United States of America</i>, <i>117</i>(38), 23393–23400. https://doi.org/10.1073/PNAS.1914950117/SUPPL_FILE/PNAS.1914950117.SAPP.PDF</div>
&nbsp;
<div class="csl-entry">Lichtenwalter, R. N., Lussier, J. T., &#38; Chawla, N. v. (2010). <i>New Perspectives and Methods in Link Prediction</i>.</div>
&nbsp;
<div class="csl-entry">Freund, Y., &#38; Schapire, R. E. (1999). A Short Introduction to Boosting. <i>Journal of Japanese Society for Artificial Intelligence</i>, <i>14</i>(5), 771–780. www.research.att.com/fyoav,</div>


